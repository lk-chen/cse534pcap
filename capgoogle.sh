mkdir 'pcap' 

# capture quic on google
rm $HOME/.cache/google-chrome/Default/Cache/ -rf 
tshark -i eth0 -f "udp port 443" -w pcap/quic_google.pcap & 
google-chrome --enable-quic --use-spdy=off --origin-to-force-quic-on=www.google.com:443 https://www.google.com/#q=google & 
sleep 5s | echo 'sleep for 5s' 
sleep 5s | echo 'sleep for 5s' 
sleep 5s | echo 'sleep for 5s' 
sleep 5s | echo 'sleep for 5s' 
killall chrome 
killall tshark 

# capture quic on youtube
rm $HOME/.cache/google-chrome/Default/Cache/ -rf 
tshark -i eth0 -f "udp port 443" -w pcap/quic_youtube.pcap & 
google-chrome --enable-quic --use-spdy=off --origin-to-force-quic-on=www.youtube.com:443 https://www.youtube.com/ & 
sleep 5s | echo 'sleep for 5s' 
sleep 5s | echo 'sleep for 5s' 
sleep 5s | echo 'sleep for 5s' 
sleep 5s | echo 'sleep for 5s' 
killall chrome 
killall tshark 

# capture http on google
rm $HOME/.cache/google-chrome/Default/Cache/ -rf 
tshark -i eth0 -f "tcp port 443" -w pcap/http_google.pcap & 
google-chrome --use-spdy=off https://www.google.com/#q=google & 
sleep 5s | echo 'sleep for 5s' 
sleep 5s | echo 'sleep for 5s' 
sleep 5s | echo 'sleep for 5s' 
sleep 5s | echo 'sleep for 5s' 
killall chrome 
killall tshark 

# capture http on youtube
rm $HOME/.cache/google-chrome/Default/Cache/ -rf 
tshark -i eth0 -f "tcp port 443" -w pcap/http_youtube.pcap & 
google-chrome --use-spdy=off https://www.youtube.com & 
sleep 5s | echo 'sleep for 5s' 
sleep 5s | echo 'sleep for 5s' 
sleep 5s | echo 'sleep for 5s' 
sleep 5s | echo 'sleep for 5s' 
killall chrome 
killall tshark 

# capture spdy on google
tshark -i eth0 -f "tcp port 443 and host www.google.com" -w pcap/spdy_google.pcap & 
firefox --use-spdy=no-ssl https://www.google.com/#q=google & 
sleep 5s | echo 'sleep for 5s' 
sleep 5s | echo 'sleep for 5s' 
sleep 5s | echo 'sleep for 5s' 
sleep 5s | echo 'sleep for 5s' 
killall firefox 
killall tshark 

# capture spdy on youtube
tshark -i eth0 -f "tcp port 443 and host www.youtube.com" -w pcap/spdy_youtube.pcap & 
firefox --use-spdy=no-ssl https://www.youtube.com & 
sleep 5s | echo 'sleep for 5s' 
sleep 5s | echo 'sleep for 5s' 
sleep 5s | echo 'sleep for 5s' 
sleep 5s | echo 'sleep for 5s' 
killall firefox 
killall tshark 



