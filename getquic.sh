cd ~
git clone https://chromium.googlesource.com/chromium/tools/depot_tools.git
export PATH=$PATH:$HOME/depot_tools
mkdir chromium
cd chromium
fetch --nohooks --no-history chromium
sudo ./src/build/install-build-deps.sh
gclient runhooks
cd src/
gn gen out/Debug
ninja -C out/Debug quic_server quic_client
mkdir /tmp/quic-data
cd /tmp/quic-data
wget -p --save-headers https://www.example.org
cd -
cd net/tools/quic/certs
./generate-certs.sh
cd -
sudo apt-get install libnss3-tools
mkdir -p ~/.pki/nssdb
certutil -N -d sql:$HOME/.pki/nssdb
certutil -d sql:$HOME/.pki/nssdb -A -t "C,," -n quic \
-i net/tools/quic/certs/out/2048-sha256-root.pem
certutil -d sql:$HOME/.pki/nssdb -L
cd ~


