sudo apt-get install apache2-2.2.22-1ubuntu1.10 apache2.2-bin apache2.2-common
sudo a2enmod ssl
sudo service apache2 restart
wget https://dl-ssl.google.com/dl/linux/direct/mod-spdy-beta_current_amd64.deb
sudo dpkg -i mod-spdy-beta_current_amd64.deb
sudo apt-get -f install
rm mod-spdy-beta_current_amd64.deb
sudo service apache2 restart
cp ./nosslspdy.conf /etc/apache2/mods-available/spdy.conf

sudo apt-get install zsh git
chsh -s /bin/zsh
git clone git://github.com/robbyrussell/oh-my-zsh.git ~/.oh-my-zsh
cp ~/.oh-my-zsh/templates/zshrc.zsh-template ~/.zshrc

wget -p https://spdy.centminmod.com/flags.html
sudo cp spdy.centminmod.com/* /var/www -rf
sudo service apache restart
