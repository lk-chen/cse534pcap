import os
import sys

def gen_http_html(n, pre):
    content = "<!DOCTYPE html>\n<html><body>\n"
    content += "<p>test page with raw objs</p>\n"
    
    for i in range(n):
        content += '<script type="text/javascript" src="js/'+pre+'-'+str(i)+'.js"></script>\n'
    
    content += "</body></html>\n"
    
    f = open("httpobj/test.html", 'w')
    f.write(content)
    f.close()
    
def gen_quic_html(n, pre):
    header = """HTTP/1.1 200 OK\r
X-original-url: www.example.org/test.html\r
Accept-Ranges: bytes\r
Cache-Control: max-age=604800\r
Content-Type: text/html\r
Date: Wed, 30 Apr 2016 00:00:00 GMT\r
Etag: ""\r
"""
    content = "<!DOCTYPE html>\n<html><body>\n"
    content += "<p>test page with raw objs</p>\n"
    
    for i in range(n):
        content += '<script type="text/javascript" src="https://www.example.org/js/'+pre+'-'+str(i)+'.js"></script>\n'
    
    content += "</body></html>\n"
    
    header += "Content-Length: " + str(len(content)) + "\r\n\r\n"
        
    f = open("quicobj/test.html", 'w')
    f.write(header+content)
    f.close() 
    
def gen_http_js(i, sz, pre):
    content = '/*'
    content += '#' * (sz-4)
    content += '*/'
    
    f = open("httpobj/js/"+pre+'-'+str(i)+'.js', 'w')
    f.write(content)
    f.close()
        
def gen_quic_js(i, sz, pre):
    header = """HTTP/1.1 200 OK\r
X-original-url: www.example.org/js/"""+pre+'-'+str(i)+""".js\r
Accept-Ranges: bytes\r
Cache-Control: max-age=604800\r
Content-Type: text/javascript\r
Date: Wed, 30 Apr 2016 00:00:00 GMT\r
Etag: ""\r
"""
    content = '/*'
    content += '#' * (sz-4)
    content += '*/'
    
    header += "Content-Length: " + str(len(content)) + "\r\n\r\n"
    
    f = open("quicobj/js/"+pre+'-'+str(i)+'.js', 'w')
    f.write(header+content)
    f.close()
        
if __name__=="__main__":
    os.system("rm quicobj -rf")
    os.system("mkdir quicobj")
    os.system("mkdir quicobj/js")
    
    os.system("rm httpobj -rf")
    os.system("mkdir httpobj")
    os.system("mkdir httpobj/js")

    n = 8
    if len(sys.argv) > 1:
        n = int(sys.argv[1])
    sz = 1000
    if len(sys.argv) > 2:
        sz = int(sys.argv[2])
    pre = ''
    if sz >= 1000000:
        pre = '1M'
    elif sz < 1000:
        pre = '100B'
    else:
        pre = str(sz/1000)+'K'
    gen_quic_html(n, pre)
    gen_http_html(n, pre)
    for i in range(n):
        gen_quic_js(i, sz, pre)
        gen_http_js(i, sz, pre)
