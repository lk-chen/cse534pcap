cd ~
cp /etc/apache2/mods-available/spdy.conf spdy.conf.backup
wget http://wprof.cs.washington.edu/spdy/tool/server.tar.gz
tar -xvf server.tar.gz
sudo cp -r server/* /var/www/
rm server.tar.gz
rm server/ -rf
sudo apt-get install nodejs npm
sudo npm cache clean -f
npm config set registry http://registry.npmjs.org/
sudo npm install -g n
sudo n stable
sudo npm install spdy
wget http://wprof.cs.washington.edu/spdy/tool/epload.tar.gz
tar -xvf epload.tar.gz
wget http://wprof.cs.washington.edu/spdy/tool/dependency_graphs.tar.gz
tar -xvf dependency_graphs.tar.gz
rm epload.tar.gz
rm dependency_graphs.tar.gz
git clone https://github.com/lk-chen/cs244-pa3.git
sudo cp cs244-pa3/rawobj.com/ /var/www/ -r
cp cs244-pa3/epload/client_spdy/spdy_client/spdy_client/session.js epload/client_spdy/spdy_client/spdy_client/
node epload/emulator/run.js http epload/emulator/tests > result.txt
