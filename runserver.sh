cd $HOME/chromium/src
./out/Debug/quic_server \
--quic_in_memory_cache_dir=/tmp/quic-data/www.example.org/ \
--certificate_file=net/tools/quic/certs/out/leaf_cert.pem \
--key_file=net/tools/quic/certs/out/leaf_cert.pkcs8 & 
