rm /tmp/chrome-profile/Cache/* -rf
google-chrome \
--user-data-dir=/tmp/chrome-profile \
--no-proxy-server \
--enable-quic \
--origin-to-force-quic-on=www.example.org:443 \
--host-resolver-rules='MAP www.example.org:443 10.0.0.2:6121' \
https://www.example.org/test.html   
