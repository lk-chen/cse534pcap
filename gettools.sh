sudo apt-get install apache2-2.2.22-1ubuntu1.10 apache2.2-bin apache2.2-common
sudo a2enmod ssl
sudo service apache2 restart
wget https://dl-ssl.google.com/dl/linux/direct/mod-spdy-beta_current_amd64.deb
sudo dpkg -i mod-spdy-beta_current_amd64.deb
sudo apt-get -f install
rm mod-spdy-beta_current_amd64.deb
sudo service apache2 restart
cd ~
git clone git://github.com/mininet/mininet
cd mininet
git checkout -b 2.1.0p2
cd -
mininet/util/install.sh
cd -
sudo mn --test pingall
sudo apt-get install wireshark libcap2-bin
sudo dpkg-reconfigure wireshark-common
sudo adduser $USER wireshark
sudo chgrp wireshark /usr/bin/dumpcap
sudo chmod 750 /usr/bin/dumpcap
sudo setcap cap_net_raw,cap_net_admin=eip /usr/bin/dumpcap
sudo apt-get install zsh git
chsh -s /bin/zsh
git clone git://github.com/robbyrussell/oh-my-zsh.git ~/.oh-my-zsh
cp ~/.oh-my-zsh/templates/zshrc.zsh-template ~/.zshrc
