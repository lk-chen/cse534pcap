for n in 16 128 ; do
    for sz in 1000 1000000 ; do
        python gen_pg.py $n $sz
        cp quicobj/* /tmp/quic-data/www.example.org/ -rf
        sudo cp httpobj/* /var/www/ -rf
        rm quicobj/ -rf
        rm httpobj/ -rf
        sudo python run.py $n $sz 
    done
done
