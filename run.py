#!/usr/bin/python

from mininet.topo import Topo
from mininet.node import CPULimitedHost
from mininet.link import TCLink
from mininet.net import Mininet
from mininet.term import makeTerms
from mininet.log import setLogLevel
from time import time, sleep
from threading import Timer

import os
import sys
import subprocess
import json

args = {}

class ExpTopo(Topo):
    "Simple IP topology"
    # rtt(ms), bw(Mb/s), loss(%)
    
    def build(self):
        global args
        n = args['n']
        
        switch = self.addSwitch('s0')
        hosts = []
        for i in range(n):
            hosts.append(self.addHost('h'+str(i+1)))
        h1 = hosts[0]
        
        delay = str(args['rtt']/2) + 'ms'
        bw = args['bw']
        loss = args['loss']
        link_opts = dict(bw=bw, loss=loss, delay=delay)
        self.addLink(h1, switch, **link_opts)
        for i in range(1, n):
            self.addLink(hosts[i], switch)
            
        return
        
def http(net):
    h2 = net['h2']
    h2.cmd("sudo service apache2 restart")
    
    h1 = net['h1']
    h1.cmd('firefox http://10.0.0.2/test.html ')
    
    return
    
def spdy(net):
    h2 = net['h2']
    h2.cmd("sudo service apache2 restart")
    
    h1 = net['h1']
    h1.cmd('./spdychrome.sh')
    
    sleep(15)
    
    return
        
threadNum = 0

def mimicDl(dg, id, host):
    global threadNum
    depended = [obj for obj in dg['objs'] if obj['download']['id'] == id][0]
    
    host.cmd('./chromium/src/out/Debug/quic_client --host='+depended['host']+' --port=6121 https://'+depended['path'])
    for comp in depended['comps']:
        triggers = [obj for obj in dg['deps'] if obj['a1'] == comp['id']]
        for trigger in triggers:
            threadNum += 1
            if trigger['time'] < 0:
                Timer(comp['time']/1000, mimicDl, (dg, trigger['a2'], host)).start()
            else:
                Timer(trigger['time']/1000, mimicDl, (dg, trigger['a2'], host)).start()
    threadNum -= 1
    
    return

def quic(net):
    h2 = net['h2']
    h2.cmd("./runserver.sh")
    
    h1 = net['h1']
    print h1.cmd('./quicchrome.sh')
    
    return

def args2str(args):
    res = args['protocol']
    for p in 'rtt','bw','loss','url':
        res = res + '_' + p + '=' + str(args[p])
        
    return res

def run(args):
    if not os.path.exists(args['out']):
        os.makedirs(args['out'])
    os.system("rm ~/.cache/google-chrome/Default/Cache/* -rf")
    # os.system("sudo mn -c > /dev/null")
    # os.system("sysctl -w net.ipv4.tcp_congestion_control=%s" % args['cong'])
    topo = ExpTopo()
    net = Mininet(topo=topo, link=TCLink)
    
    net.start()
    
    h1 = net['h1']
    filename = args['out'] + args2str(args) + '_ts=' + str(int(time())) + '.pcap'
    makeTerms([h1], '')
    
    if args['protocol'] == 'http':
        print h1.cmd('tshark -i h1-eth0 -w ' + filename +' -f "tcp port 80" &')
        http(net)
    elif args['protocol'] == 'spdy':
        print h1.cmd('tshark -i h1-eth0 -w ' + filename +' -f "tcp port 80" &')
        spdy(net)
    elif args['protocol'] == 'quic':
        print h1.cmd('tshark -i h1-eth0 -w ' + filename +' -f "udp port 6121" &')
        quic(net)
    
    h1.cmd('killall xterm')
    sleep(1)
    
    h1.cmd('chmod 666 ' + filename)
    net.stop()
    
if __name__ == "__main__":
    args['n'] = 2
    args['out'] = "pcap/"
    args['cong'] = "Cubic"
    args['rtt'] = 100
    args['bw'] = 10
    args['loss'] = 0
    args['protocol'] = 'quic'
    args['url'] = 'rawjsexample'
    
    if len(sys.argv) > 2:
        args['url'] = sys.argv[1] + '_' + sys.argv[2]
        args['bw'], args['rtt'] = 0.75, 100 #Regular 3G
        run(args)
        args['bw'], args['rtt'] = 1, 40 #Good 3G
        run(args)
        args['bw'], args['rtt'] = 4, 20 #Regular 4G
        run(args)
        args['bw'], args['rtt'] = 30, 2 #WiFi
        run(args)
    else:
        run(args)
    
    
